---
title: "Design"
date: 2022-07-18T11:17:05+03:00
---

System design is a detailed description of SIDLOC specification and experiments, including the composition, functions and rationale behind any design choices.
It is based on a detailed analysis of [system requirements]({{<ref requirements.md>}}).

## System Design Document ##

The System Design Document is maintained as a LaTeX document.
The document is authored under [SIDLOC Overleaf project](https://www.overleaf.com/project/60be9d12dcf6a9d028e041b1) and [mirrored](https://gitlab.com/librespacefoundation/sidloc/sidloc-docs) to GitLab where it is rendered into a PDF file using [GitLab CI](https://docs.gitlab.com/ee/ci/).

### Releases ###

- [Unstable](https://gitlab.com/librespacefoundation/sidloc/sidloc-docs/-/jobs/artifacts/master/raw/build/design-document.pdf?job=docs_devel)
