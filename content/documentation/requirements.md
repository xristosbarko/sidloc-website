---
title: "Requirements"
date: 2021-07-12T22:48:14+03:00
---

High-level system requirements for SIDLOC are identified based on specified use cases and stakeholders' input.
The list of requirements is necessary in order to provide implementers a clear view and understanding of the system under development as well as defining standard interfaces for users of the system.

## Management and Tracking ##

SIDLOC requirements are managed on GitLab under [SIDLOC Requirements](https://gitlab.com/librespacefoundation/sidloc/sidloc-requirements) project.
Each requirement is filed as a GitLab issue, with the `Requirements` label attached.
Additional labels shall also be attached based on the requirement types that are applicable to it.
A `Requirement` [issue template](https://docs.gitlab.com/ee/user/project/description_templates.html) is available for creating requirement issues conveniently and consistently.
A requirement shall always relate to one or more use cases by [linked GitLab issues](https://docs.gitlab.com/ee/user/project/issues/related_issues.html), with the exception of design-only requirements.
This use cases - requirement relations are used for origin tracking and quality assurance.

## Specification Document ##

The System Requirements Specification is maintained as a LaTeX document.
The document is authored under [SIDLOC Overleaf project](https://www.overleaf.com/project/60be9d12dcf6a9d028e041b1) and [mirrored](https://gitlab.com/librespacefoundation/sidloc/sidloc-docs) to GitLab where it is rendered into a PDF file using [GitLab CI](https://docs.gitlab.com/ee/ci/).
The list of requirements contained in the document is populated directly from GitLab issues using the [GitLab API](https://docs.gitlab.com/ee/api/).
Addition, deletion or modification of any issue, automatically triggers the re-rendering of the PDF file.

### Releases ###

- [Unstable](https://gitlab.com/librespacefoundation/sidloc/sidloc-docs/-/jobs/artifacts/master/raw/build/requirements-specification.pdf?job=docs_devel)
- Stable
  - [1.2.0](https://gitlab.com/librespacefoundation/sidloc/sidloc-docs/-/jobs/artifacts/1.2.0/raw/build/requirements-specification.pdf?job=docs_release)
  - [1.1.0](https://gitlab.com/librespacefoundation/sidloc/sidloc-docs/-/jobs/artifacts/1.1.0/raw/build/requirements-specification.pdf?job=docs_release)
  - [1.0.0](https://gitlab.com/librespacefoundation/sidloc/sidloc-docs/-/jobs/artifacts/1.0.0/raw/build/requirements-specification.pdf?job=docs_release)

## List of Requirements ##

{{% requirement %}}
