---
title: "Advisory Board"
date: 2021-06-30T12:03:36+03:00
---

It is important to have the broadest industry and stakeholder input and buy-in for the SIDLOC standard to be successful.
This will ensure that key input and verification of requirements will take place early on development, and early experiments will be representative of the industry needs.
Furthermore, with the prospect of SIDLOC evolving and being adopted as an industry standard, it is important to form as early as possible a consensus among the prospective users, implementers, regulators and any other relevant stakeholders.
To that end 7 different categories of stakeholders are identified as follows:

## Satellite Integrators ##

In this category we include satellite builders and integrators of various different target sizes, target usages and scale of operations.
There could be vertical integrators or COTS or custom sub-system providers.
Specific to this group we are expecting input on the technology integration requirements, with regards to power budget, placement, mass, access to satellite bus and other integration specificities.

## Launch Providers ##

In this category we include entities that are facilitating launches by either providing launch broker services or directly conducting launches themselves.
Specific to this group we are expecting input and validation about the LEOP identification needs and their perspective on the applicability of the technology to novel launching platforms and paradigms.

## Ground Segment Providers ##

In this category we include entities that provide ground segment services, either by owning or brokering ground segments.
Specific to this group we are expecting input and validation of our proposed Operational concept with regards to the ground segment and also the compatibility with existing systems and technologies.

## Insurers ##

In this category we include entities that provide insurance services for space missions.
Specific to this group we are expecting input on the high-level verification of the technology as well as how such identification technology might impact their service offerings and pricing structure.

## SSA Providers ##

In this category we include entities that provide Space Situational Awareness and related services.
Specific to this group we are expecting a lot of interactions, input and validation since our proposed development is in the core of their scope.
Additionally, input on the business models emerging from an adoption of a standard will be a key discussion point with them.

## Regulators ##

In this category we include entities that act in a regulatory capacity on a national or international level with regards to the spectrum or general space domain.
Their specific input on regulatory aspects, processes and adoption pathways will be crucial for the framing of our technology development as well as its prospective adoption.

## Researchers ##

In this category we include entities that have proposed or developed and tested technologies related to satellite identification or within the broader SSA domain.
Specific to this group we are expecting validation on our pros/cons analysis of the existing developments as well as input for the flexibility and adoption of our proposed development.
